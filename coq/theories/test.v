Require Import utiles.
Require Import jeu.
Require Import strategy.
Require Import residu.
Require Import jeuthese.




Lemma  cons_inj `{J:Game} `{G:Game}:
  forall a a' m m' p p' P P' H,
    @consO_l J G a m p P = consO_l a' m' p' P'
    -> H a m p P -> H a' m' p' P'.
Proof.
  intros.
  inversion H0.
  Print ord.
Admitted.


Section Test.
Parameter A:Type.
Parameter myord: A -> A -> Prop.
Definition is_min a:= forall b, myord b a -> a=b.


Inductive mylist :=
| mynil : mylist
| mycons : forall b, is_min b -> mylist.

(* Set Keep Proof Equalities. *)

Lemma cons_inj2:
  forall  b b' m m' P,
    mycons b m =  mycons b' m'
      -> P b m -> P b' m'.
Proof.
  intros.
  inversion H.
  (* subst. *)
  (* assert (Inj:=Eqdep.EqdepTheory.inj_pair2 A (fun b : A => is_min b) b' m m' H2). *)
  assumption.
Qed.




End Test.
